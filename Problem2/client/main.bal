import ballerina/http;
import ballerina/io;

public function main() returns error? {
    // Creates a new client with the backend URL.
    final http:Client clientEndpoint = check new ("http://localhost:4000");

    check LoginUI(clientEndpoint);
}

function LoginUI(http:Client clientEndpoint) returns error? {
    io:println("****************Welcome*****************");
    io:println("****************************************");
    io:println("What would you like to do?");
    io:println("1. Get stats");
    io:println("2. Get by ISO");
    io:println("3. Update stats");
    io:println("****************************************");
    string choice = io:readln("Enter choice 1-3: ");

    json resp;

    if (choice == "1") {
        resp = check clientEndpoint->post("/graphql", {query: "{ all { region cases active } }"});
        io:println(resp.toJsonString());
    } else if (choice == "2") {
        string iso = io:readln("Enter ISO: ");
        resp = check clientEndpoint->post("/graphql", {query: "{ filter(isoCode: \"" + iso + "\" ) { region cases } }"});
        io:println(resp.toJsonString());

    } else if (choice == "3") {
        string iso = io:readln("Enter ISO: ");
        string cases = io:readln("Enter new cases: ");
        string deaths = io:readln("Enter deaths: ");
        string recovery = io:readln("Enter recoveries: ");

        resp = check clientEndpoint->post("/graphql", {query: "mutation{ update(isoCode: \""+iso+"\", entry: {deaths: "+deaths+", recovered: "+recovery+", new_case: "+cases+"}) { region cases recovered deaths active } }"});
        io:println(resp.toJsonString());
    }


    check LoginUI(clientEndpoint);

}
