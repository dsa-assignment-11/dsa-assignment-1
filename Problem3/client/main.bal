import ballerina/io;
import ballerina/http;


public function main() returns error? {
    
    final http:Client clientEndpoint = check new ("http://localhost:4000"); // Creates a new client with the backend URL.

    while (true) {

        io:println(" OPTIONS: ");
        io:println("1. Create student");
        io:println("2. Delete student");
        io:println("3. Update student details");
        io:println("4. Look up student");
        io:println("5. Update students course details");
        io:println("6. Get all students");
        io:println("    ");

        string choice = io:readln("Enter Options 1-6: ");
        io:println("    ");

        if (choice == "1") {
            string id = io:readln("Enter student number: ");
            string name = io:readln("Enter name: ");
            string email = io:readln("Enter email: ");
            string address = io:readln("Enter address: ");

            json resp = check clientEndpoint->post("/create", {studentNum: id, name: name, email: email, address: address});
            io:println(resp.toJsonString());
            io:println("    ");

        } else if (choice == "2") {
            string studentNum = io:readln("Enter student number: ");
            json resp = check clientEndpoint->delete("/delete?studentNum=" + studentNum);
            io:println(resp);
            io:println("    ");

        }else if (choice == "3") {
            string studentNum = io:readln("Enter student number: ");

            io:println("Please Enter new information or leave blank to skip/move-on.");
            string name = io:readln("Enter name: ");
            string email = io:readln("Enter email: ");
            string address = io:readln("Enter address: ");

            json resp = check clientEndpoint->put("/update", {studentNum: studentNum, name: name, email: email, address: address});
            io:println(resp.toJsonString());    
            io:println("    ");

        } else if (choice == "4") {
            string studentNum = io:readln("Enter student number: ");

            json resp = check clientEndpoint->get("/lookup?studentNum=" + studentNum);
            io:println(resp);
            io:println("    ");

        }else if (choice == "5") {
            string studentNum = io:readln("Enter student number: ");
            string course = io:readln("Enter course code: ");

            json resp = check clientEndpoint->put("/course", {studentNum: studentNum, course: course});
            io:println(resp.toJsonString());
            io:println("    ");

        }  else if (choice == "6") {
            json resp = check clientEndpoint->get("/all");
            io:println(resp);
            io:println("    ");

        }
    }
}



