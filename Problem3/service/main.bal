import ballerina/http;

import ballerina/io;

public type Student record {|
    readonly string studentNum;
    string email;
    string name;
    string address;
    string[] courses = [];
|};

public type Course record {|
    readonly string code;
    table<Ass> assigns;
|};

public type Sub record {|
    readonly string studentNum;
    float mark;
    string content;
|};

public type Ass record {|
    readonly string id;
    float weight;
    table<Sub> subs;
|};

table<Student> key(studentNum) students = table [
        {studentNum: "217061141", name: "Klievett", email: "klievettnicarlo@gmail.com", address: "", courses: ["AI611s"]}];

table<Course> key(code) courses = table [
    {
    code: "AI611s",
    assigns: table [
        {
            id: "3001", weight: 0.5, subs: table [
                {
                studentNum: "217061141",
                content: "This is the Submission",
                mark: 0.0
                }
            ]
        }
    ]
    }
];  

public type RequestCourse record {|
    string studentNum;
    string course;
|};

public type RequestStudent record {|
    string name = "";
    string studentNum;
    string email = "";
    string address = "";
|};


service / on new http:Listener(4000) {
    
    resource function post create(@http:Payload Student payload) returns json {   // path to create student account
        // string? studentNum = payload.studentNum;
        students.add({studentNum: payload.studentNum, name: payload.name, email: payload.email, address: payload.address, courses: []});

        io:println(students.toBalString());
        return {success: true, message: " *** Student created successfully *** "};
    }

    resource function put update(@http:Payload RequestStudent payload) returns json {

        io:println(payload.toBalString());
        Student student = students.get(payload.studentNum);

        if (payload.name !== "") {
            student.name = payload.name;
        }   

        if (payload.address !== "") {
            student.address = payload.address;

        }

        if (payload.email !== "") {
            student.email = payload.email;

        }
        return {success: true, message: " *** Updated student details ***" };

    }

     resource function get all() returns json {
        return students.toJson();
    }

    resource function delete delete(string studentNum) returns json {
        Student result = students.remove(studentNum);

        io:println(students.toBalString());
        return {success: true, message: "*** Student removed ***"};

    }

    resource function put course(@http:Payload RequestCourse payload) returns json {

        Student student = students.get(payload.studentNum);
        student.courses.push(payload.course);

        return {success: true, message: "*** Added course ***"};
    }

    resource function get lookup(string studentNum) returns Student {
        return students.get(studentNum);
    }

   

}
